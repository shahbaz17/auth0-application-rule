var express = require('express');
var secured = require('../lib/middleware/secured');
var dotenv = require('dotenv');
var router = express.Router();

dotenv.load();

const fetch = require("node-fetch");
const application_data = [];
const rules_data = [];
const output = {};

async function getToken(){

	try {
      // this parse may fail
      const url = 'https://'+process.env.AUTH0_DOMAIN+'/oauth/token';
      const audience = 'https://'+process.env.AUTH0_DOMAIN+'/api/v2/';
	    const res = await fetch(url, {method: 'POST', headers: { 'content-type': 'application/json' }, body: '{"client_id":"'+process.env.API_EXPLORER_APP_CLIENT_ID+'","client_secret":"'+process.env.API_EXPLORER_APP_CLIENT_SECRET+'","audience":"'+audience+'","grant_type":"client_credentials"}' });
      const data = await res.json();
      const access_token = data.access_token;
      // console.log(access_token);
      await getApplication(access_token);
      await getRules(access_token);
      justDisplay();
	  } catch (err) {
	    console.log(err)
	}
}

async function getApplication(access_token){
	try {
      const url = 'https://'+process.env.AUTH0_DOMAIN+'/api/v2/clients';
	    const res = await fetch(url, {method: 'GET', headers: { authorization: 'Bearer '+access_token } });
      const data = await res.json();
      // console.log(data);
      this.application_data = data;
	  } catch (err) {
	    console.log(err)
	}
}

async function getRules(access_token){
	try {
      const url = 'https://'+process.env.AUTH0_DOMAIN+'/api/v2/rules';
	    const res = await fetch(url, {method: 'GET', headers: { authorization: 'Bearer '+access_token } });
      const data = await res.json();
      // console.log(data);
      this.rules_data = data;
	  } catch (err) {
	    console.log(err)
	}
}

async function justDisplay(){
	var application_list = []; // Array containing Applicaton list
	var rules_obj = {}; // Object containing {rules: application} object

	this.application_data.forEach(function(element) {
	  application_list.push(element.name);
	});
	for (const key of this.rules_data) {
		const app_name = key.script.split("context.clientName")[1].split("'")[1];
		rules_obj[key.name] = app_name; // rule_name : application_name
	}

	const result_output = {}; // output object

	for(key in rules_obj) {
	  if(rules_obj[key] in result_output) {
	    result_output[rules_obj[key]].push(key);
	  }
	  else {
	    result_output[rules_obj[key]] = [key];
	  }
	}
  //console.log(result_output);
  outputData(result_output);
}

getToken();

function outputData(result_output){
  this.output = result_output;
  // console.log(process.env.AUTH0_DOMAIN);
}


/* GET user profile. */
router.get('/applications_rules', secured(), function (req, res, next) {
  
  res.render('applications_rules', { 
      outputData: JSON.stringify(this.output, null, 2),
      title: 'Application-Rules | Auth0 Webapp sample Nodejs' 
    });
});

module.exports = router;
